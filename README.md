# Home Assistant OS X power notification daemon

This is a small utility that hooks into [IOKit's power management](https://developer.apple.com/library/archive/documentation/DeviceDrivers/Conceptual/IOKitFundamentals/PowerMgmt/PowerMgmt.html) to receive notifications for power and display events.
This gets translated into three states that can be reported via either MQTT or the Home Assistant REST API:

- `awake`
- `display_dim` which mostly just means the screen is off but the machine is still awake
- `sleep` which also includes power off, but there is no separate notification for that transition

Since my new job involves me learning [Go](https://golang.org/), I figured this might as well be a good time to learn how [cgo](https://golang.org/cmd/cgo/) works. This is therefore also an example of how to wrap native C code for macOS into Go, as well as how to set up a GitLab pipeline that can use a shell executor in a GitLab runner running on a Mac to compile code for OS X specifically.

Inspiration comes from [sleepwatcher](https://www.bernhard-baehr.de/) which allows you to define executables for every eventuality; I was looking for something lightweight that can be dedicated to home automation. Also, [this gist](https://gist.github.com/nicolai86/fd720c9a660188fe29d7ac7a810f96f3) was a good starting point for the cross-language work and seemed to be doing the exact same thing I was looking for, but it needed a bit of work to actually make it functional.

Power events are reported either via MQTT or REST, and you can define either or both parameters as needed. If left undefined, this tool won't attempt a notification.

```
  -api_auth string
    	Home Assistant API endpoint bearer token, a.k.a. Long-Lived Access Token (might not be needed if you use local-network authentication)
  -api_endpoint string
    	Home Assistant API endpoint, pointing to the exact entity whose state must be updated, e.g.: http://localhost:8123/api/states/sensor.imac_state
  -mqtt_broker string
    	MQTT broker URI, e.g.: tcp://localhost:1883
  -mqtt_clean
    	MQTT Set Clean Session (default false)
  -mqtt_id string
    	MQTT ClientID (optional) (default "osx_power_notify")
  -mqtt_password string
    	MQTT password (optional)
  -mqtt_qos int
    	MQTT Quality of Service 0,1,2 (default 0)
  -mqtt_topic string
    	MQTT topic name to/from which to publish/subscribe
  -mqtt_user string
    	MQTT username (optional)
```

## Installation

Being on OS X, I'm going to assume you've set up [homebrew](https://brew.sh/) already. You can build and install this by executing `brew tap dewet/homebrew-tap https://gitlab.com/dewet/homebrew-tap` followed by `brew install home-assistant-osx-power-notify`. I'll try to keep it updated in sync.

A sample [.plist](org.dewet.powernotify.plist) in the repo can be installed to `/Library/LaunchDaemons` along with running `sudo launchctl load /Library/LaunchDaemons/org.dewet.powernotify.plist` to have launchctl take care of keeping it up and running system-wide. This is left as an exercise for the reader.
