package main

import "C"

// HandleWake handles a 'wake' event
//export HandleWake
func HandleWake() {
	PowerState = "awake"
	DoPost()
}

// HandleSleep handles a 'sleep' event
//export HandleSleep
func HandleSleep() {
	PowerState = "sleep"
	DoPost()
}

// HandlePowerOn handles a 'power on' event
//export HandlePowerOn
func HandlePowerOn() {
	PowerState = "awake"
	DoPost()
}

// HandleDisplayDim handles a 'display dim' event
//export HandleDisplayDim
func HandleDisplayDim() {
	// Screen off / low power before sleeping
	PowerState = "display_dim"
	DoPost()
}

// HandleDisplaySleep handles a 'display sleep' event
//export HandleDisplaySleep
func HandleDisplaySleep() {
	// Not sure anything uses this, but treat equally to dimming
	PowerState = "display_dim"
	DoPost()
}

// HandleDisplayUndim handles a 'display undim' event
//export HandleDisplayUndim
func HandleDisplayUndim() {
	PowerState = "awake"
	DoPost()
}

// HandleDisplayWake handles a 'display wake' event
//export HandleDisplayWake
func HandleDisplayWake() {
	PowerState = "awake"
	DoPost()
}
