module gitlab.com/dewet/home-assistant-osx-power-notify

go 1.14

require gopkg.in/resty.v1 v1.12.0

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mitchellh/go-homedir v1.1.0
	gopkg.in/yaml.v2 v2.3.0
)
