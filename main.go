package main

// #cgo LDFLAGS: -framework CoreFoundation -framework IOKit
// void HandleWake();
// void HandleSleep();
// void HandlePowerOn();
// void HandleDisplayDim();
// void HandleDisplaySleep();
// void HandleDisplayUndim();
// void HandleDisplayWake();
// #include "main.h"
import "C"
import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/kelseyhightower/envconfig"
	homedir "github.com/mitchellh/go-homedir"
	"gopkg.in/resty.v1"
	"gopkg.in/yaml.v2"
)

const appVersion = "v2.0.0"

var (
	// Cfg holds configuration read from YAML/env
	Cfg Config
	// MqttClient is a MQTT client
	MqttClient mqtt.Client
	// PowerState keeps the current power state
	PowerState = "awake" // safe to assume this default at start-up?
)

// Config describes the YAML/environment configuration
type Config struct {
	Refresh int `yaml:"refresh" envconfig:"REFRESH"`
	Rest    struct {
		APIEndpoint string `yaml:"apiEndpoint" envconfig:"REST_API_ENDPOINT"`
		BearerToken string `yaml:"bearerToken" envconfig:"REST_BEARER_TOKEN"`
	} `yaml:"rest"`
	Mqtt struct {
		Broker       string `yaml:"broker" envconfig:"MQTT_BROKER"`
		Topic        string `yaml:"topic" envconfig:"MQTT_TOPIC"`
		Username     string `yaml:"username" envconfig:"MQTT_USERNAME"`
		Password     string `yaml:"password" envconfig:"MQTT_PASSWORD"`
		ClientID     string `yaml:"clientId" envconfig:"MQTT_CLIENT_ID"`
		CleanSession bool   `yaml:"cleanSession" envconfig:"MQTT_CLEAN_SESSION"`
		Qos          int    `yaml:"qos" envconfig:"MQTT_QOS"`
	} `yaml:"mqtt"`
}

// DoPost posts to API / MQTT endpoints
func DoPost() {
	if Cfg.Rest.APIEndpoint != "" {
		resty.SetTimeout(5 * time.Second)
		log.Printf("Reporting state [%s] via REST API", PowerState)
		_, err := resty.R().
			SetHeader("Authorization", fmt.Sprintf("Bearer %s", Cfg.Rest.BearerToken)).
			SetHeader("Content-Type", "application/json").
			SetBody(map[string]interface{}{"state": PowerState}).
			Post(Cfg.Rest.APIEndpoint)
		if err != nil {
			log.Printf("REST API call failed: %s", err)
		}
	}

	if MqttClient != nil {
		log.Printf("Reporting state [%s] via MQTT", PowerState)
		if token := MqttClient.Publish(Cfg.Mqtt.Topic, byte(Cfg.Mqtt.Qos), false, PowerState); token.Error() != nil {
			log.Printf("MQTT publish to %s failed: %s", Cfg.Mqtt.Topic, token.Error())
		}
	}
}

func main() {
	version := flag.Bool("version", false, "Prints current version")
	config := flag.String("config", "~/.powernotify.yaml", "Location of the configuration file")
	flag.Parse()

	if *version {
		log.Printf("powernotify %s", appVersion)
		os.Exit(0)
	}

	readFile(config)
	readEnv()
	log.Printf("powernotify %s", appVersion)
	log.Printf("Parsed config: %+v", Cfg)
	setupMqttClient()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)

	ticker := time.NewTicker(time.Duration(Cfg.Refresh) * time.Second)
	go func() {
		for {
			select {
			case <-ticker.C:
				DoPost()
			}
		}
	}()

	go func() {
		log.Print("Waiting for events.")
		C.loop()
	}()

	<-signalChan
	fmt.Println()
	log.Println("Received interrupt, shutting down.")
	ticker.Stop()
	if MqttClient != nil {
		MqttClient.Disconnect(250)
	}
}

func processError(err error) {
	log.Println(err)
	os.Exit(2)
}

func readFile(filename *string) {
	expandedFilename, err := homedir.Expand(*filename)
	if err != nil {
		processError(err)
	}
	f, err := os.Open(expandedFilename)
	if err != nil {
		processError(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&Cfg)
	if err != nil {
		processError(err)
	}
}

func readEnv() {
	err := envconfig.Process("", &Cfg)
	if err != nil {
		processError(err)
	}
}

func setupMqttClient() {
	if Cfg.Mqtt.Broker != "" && Cfg.Mqtt.Topic != "" {
		opts := mqtt.NewClientOptions()
		opts.AddBroker(Cfg.Mqtt.Broker)
		opts.SetClientID(Cfg.Mqtt.ClientID)
		opts.SetUsername(Cfg.Mqtt.Username)
		opts.SetPassword(Cfg.Mqtt.Password)
		opts.SetCleanSession(Cfg.Mqtt.CleanSession)

		MqttClient = mqtt.NewClient(opts)
		if token := MqttClient.Connect(); token.Wait() && token.Error() != nil {
			processError(token.Error())
		}
	}
}
