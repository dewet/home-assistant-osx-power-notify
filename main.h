#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

#include <mach/mach_port.h>
#include <mach/mach_interface.h>
#include <mach/mach_init.h>

#include <IOKit/pwr_mgt/IOPMLib.h>
#include <IOKit/IOMessage.h>

void powerCallback(void *rootPort, io_service_t service, natural_t messageType, void *messageArgument)
{
    switch (messageType)
    {
    case kIOMessageCanSystemSleep:
        IOAllowPowerChange(*(io_connect_t *)rootPort, (long)messageArgument);
        break;
    case kIOMessageSystemWillSleep:
        HandleSleep();
        IOAllowPowerChange(*(io_connect_t *)rootPort, (long)messageArgument);
        break;
    case kIOMessageSystemWillPowerOn:
        HandleWake();
        break;
    case kIOMessageSystemHasPoweredOn:
        HandlePowerOn();
        break;
    default:
        printf("powerCallback: Unknown messageType %08lx, arg %08lx\n",
               (long unsigned int)messageType,
               (long unsigned int)messageArgument);
        break;
    }
}

void displayCallback(void *context, io_service_t y, natural_t msgType, void *msgArgument)
{
    static enum { displayOn,
                  displayDimmed,
                  displayOff } state = displayOn;

    switch (msgType)
    {
    case kIOMessageCanDevicePowerOff:
        // ignore
        break;
    case kIOMessageDeviceWillPowerOff:
        state++;
        if (state == displayDimmed)
            HandleDisplayDim();
        else if (state == displayOff)
            HandleDisplaySleep();
        else
            printf("displayCallback: kIOMessageDeviceWillPowerOff unexpected state %04x\n", (unsigned int)state);
        break;
    case kIOMessageDeviceHasPoweredOn:
        if (state == displayDimmed)
            HandleDisplayUndim();
        else
            HandleDisplayWake();
        state = displayOn;
        break;
    default:
        printf("displayCallback: Unknown message_type %08lx, arg %08lx, state %04x\n",
               (long unsigned int)msgType, (long unsigned int)msgArgument, (unsigned int)state);
        break;
    }
}

static void registerDisplayNotifications(void)
{
    io_service_t displayWrangler;
    IONotificationPortRef notificationPort;
    io_object_t notifier;

    displayWrangler = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceNameMatching("IODisplayWrangler"));
    if (!displayWrangler)
    {
        printf("IOServiceGetMatchingService failed\n");
        exit(1);
    }
    notificationPort = IONotificationPortCreate(kIOMasterPortDefault);
    if (!notificationPort)
    {
        printf("IONotificationPortCreate failed\n");
        exit(1);
    }

    if (IOServiceAddInterestNotification(notificationPort, displayWrangler, kIOGeneralInterest,
                                         displayCallback, NULL, &notifier) != kIOReturnSuccess)
    {
        printf("IOServiceAddInterestNotification failed\n");
        exit(1);
    }
    CFRunLoopAddSource(CFRunLoopGetCurrent(), IONotificationPortGetRunLoopSource(notificationPort), kCFRunLoopDefaultMode);
    IOObjectRelease(displayWrangler);
}

static void registerPowerNotifications()
{
    io_connect_t rootPort; // a reference to the Root Power Domain IOService
    IONotificationPortRef notifyPortRef;
    io_object_t notifierObject;

    rootPort = IORegisterForSystemPower(&rootPort, &notifyPortRef, powerCallback, &notifierObject);
    if (!rootPort)
    {
        printf("IORegisterForSystemPower failed\n");
        exit(1);
    }

    CFRunLoopAddSource(CFRunLoopGetCurrent(),
                       IONotificationPortGetRunLoopSource(notifyPortRef), kCFRunLoopDefaultMode);
}

void loop()
{
    registerPowerNotifications();
    registerDisplayNotifications();
    CFRunLoopRun();
}
